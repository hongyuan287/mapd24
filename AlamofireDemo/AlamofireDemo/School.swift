//
//  School.swift
//  AlamofireDemo
//
//  Created by Michael on 2018/12/17.
//  Copyright © 2018 Zencher. All rights reserved.
//

import Foundation

struct School : Codable{
    var result:Result
}

struct Result : Codable{
    var limit:  Int
    var offset: Int
    var count:  Int
    var sort:   String
    var results:[Detail]
}

struct Detail : Codable {
    var o_tlc_agency_name : String
    var o_tlc_agency_address : String
}
