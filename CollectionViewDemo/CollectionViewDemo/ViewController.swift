//
//  ViewController.swift
//  CollectionViewDemo
//
//  Created by Michael on 2018/12/25.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var alphebat:[[String]] = [["a","b","c","d","e"],["1","2","3","4","5","6","7"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.sectionInset = UIEdgeInsets(top: 20, left: 40, bottom: 20, right: 40)

    }
    
    override func viewWillLayoutSubviews() {
        if view.bounds.size.width > view.bounds.size.height {
            widthConstraint.constant = 600
        }else {
            widthConstraint.constant = 360
        }

    }
}

extension ViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return alphebat.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return alphebat[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:CLCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CLCell", for: indexPath) as! CLCollectionViewCell
        
        cell.myLabel.text = alphebat[indexPath.section][indexPath.row]
        
        return cell
    }
    
    
}

