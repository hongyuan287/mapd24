import Vapor
import Leaf

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    
    let personController = PersonController()
    try router.register(collection: personController)
    
    
    router.get("helloLeaf") { req -> Future<View> in
        return try req.view().render("hello")
    }
    // Basic "It works" example
    router.get { req in
        return "It works!"
    }
    
    // Basic "Hello, world!" example
    router.get("hello") { req in
        return "Hello, world!"
    }
    
    
//    router.post(Person.self, at: "addPerson") { req, data -> Future<Person> in
//        
//        return data.save(on: req)
//    }
//    
//    router.get("getPeople") { req -> Future<[Person]> in
//        
//        return Person.query(on: req).all()
//    }
//    
//    router.get("getPerson", Person.parameter) { req -> Future<Person> in
//        
//        return try req.parameters.next(Person.self)
//    }
//    
//    router.put("updatePerson", Person.parameter) { req -> Future<Person> in
//        
//        return try flatMap(to: Person.self,
//                           req.parameters.next(Person.self),
//                           req.content.decode(Person.self)) { person, updatedPerson in
//                            
//                            person.name = updatedPerson.name
//                            person.age = updatedPerson.age
//                            return person.save(on: req)
//        }
//    }
//    
//    router.delete("deletePerson", Person.parameter) { req -> Future<HTTPStatus> in
//        
//        return try req.parameters.next(Person.self)
//            .delete(on: req)
//            .transform(to: HTTPStatus.noContent)
//    }

    // Example of configuring a controller
    let todoController = TodoController()
    router.get("todos", use: todoController.index)
    router.post("todos", use: todoController.create)
    router.delete("todos", Todo.parameter, use: todoController.delete)
}
