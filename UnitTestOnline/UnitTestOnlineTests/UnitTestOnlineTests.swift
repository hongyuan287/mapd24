//
//  UnitTestOnlineTests.swift
//  UnitTestOnlineTests
//
//  Created by Michael on 2018/12/18.
//  Copyright © 2018 Zencher. All rights reserved.
//

import XCTest

class UnitTestOnlineTests: XCTestCase {

    var cardView:CardView?
    
    override func setUp() {
        cardView = CardView()
        cardView?.backImage = UIImage(named: "back")!
        cardView?.frontImage = UIImage(named: "front0")!
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        cardView = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testShowBack() {
        cardView?.showBack()
//        if
        
        XCTAssertTrue(cardView?.image === cardView?.backImage, "current image is not back image")
    }
    // depend testShowBack()
    func testShowBackWhenLocked() {
        cardView?.showFront()
        cardView?.lock()
        cardView?.showBack()
        XCTAssertTrue(cardView?.image === cardView?.frontImage, "current image is not front image")
    }
    
    func testExchange(){
        cardView?.showFront()
        cardView?.exchage()
        XCTAssertTrue(cardView?.image === cardView?.backImage, "current image is not back image")
    }
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
