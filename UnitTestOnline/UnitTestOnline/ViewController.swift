//
//  ViewController.swift
//  UnitTestOnline
//
//  Created by Michael on 2018/12/18.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var cardViews: [CardView]!
    
    @IBAction func exchangeAll(_ sender: Any) {
        
        for cardView in cardViews {
            cardView.exchage()
        }
        
    }
    fileprivate func prepareData() {
        // Do any additional setup after loading the view, typically from a nib.
        for cardView in cardViews {
            cardView.backImage = UIImage(named: "back")!
            cardView.showBack()
        }
        
        for (index, cardView) in cardViews.enumerated() {
            cardView.frontImage = UIImage(named: "front\(index).png")!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareData()
        
        
    }
   
}

