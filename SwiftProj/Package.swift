// swift-tools-version:4.1
import PackageDescription
let package = Package(
    name: "SwiftProj",
    products: [
        .executable(name: "run", targets: ["Hello"])
    ],
    dependencies: [
        .package(url: "https://github.com/vapor/vapor.git", from:"3.0.0")
    ],
    targets: [
        .target(name: "Hello", dependencies:["Vapor"]),
        ]
)
