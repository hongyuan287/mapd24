//
//  CellData.swift
//  TableViewDemo
//
//  Created by Michael on 2018/12/25.
//  Copyright © 2018 Zencher. All rights reserved.
//

import Foundation

struct CellData {
    var title:String = ""
    var isSelect:Bool = false
}
