//
//  ListViewController.swift
//  TableViewDemo
//
//  Created by Michael on 2018/12/25.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController {

    var people = ["Alan", "Benson", "Calvin","David","Ethen", "Frank", "Gary", "Henry","Ivan", "Jack","Kevin","Lawrence", "Michael","Nick","Oscar","Peter","Q","Richar","Steve","Tim","Uther","Vincent","William","X","Y","Z"]
    
    var filteredArray:[String] = []

    var searchController = UISearchController(searchResultsController: nil)

    var isSearching:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        searchController.searchBar.scopeButtonTitles = ["Prefix", "Subfix"]
        self.tableView.tableHeaderView = searchController.searchBar
        searchController.dimsBackgroundDuringPresentation = false

        searchController.searchResultsUpdater = self
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            return filteredArray.count
        }
        return people.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        if isSearching {
            cell.textLabel?.text = filteredArray[indexPath.row]

        } else {
            cell.textLabel?.text = people[indexPath.row]

        }
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchingString = searchController.searchBar.text
        if searchingString == "" {
            isSearching = false
        }else {
            isSearching = true
        }
        searchText(inputString: searchingString!)
        
        self.tableView.reloadData()

    }
    
    func searchText(inputString:String){
        filteredArray = self.people.filter { (person:String) -> Bool in
            if person.uppercased().hasPrefix(inputString.uppercased()){
                return true
            }
            return false
        }
    }
}
